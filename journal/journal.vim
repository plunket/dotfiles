" To use this script for journalling in Vim, start Vim in the directory you want the
" journal files to be in with:
" gvim --servername JOURNAL -S /path/to/journal.vim
" (journal files will be put in current working directory.)

function! VerifyServerName()
    let l:unnumbered_servername = substitute(v:servername, '\d*$', '', '')
    "echo v:servername "->" l:unnumbered_servername
    if v:servername != l:unnumbered_servername
        "echo "Server name is" v:servername
        "echo "Raising" l:unnumbered_servername
        call remote_foreground(l:unnumbered_servername)
        quit
    endif
endfunction

call VerifyServerName()

let g:fix_formatting_coalesce_blank_lines = 0

function! SetUpSyntaxColoring()
    " log dates
    syntax match jnlLogDate     "\v^\d{1,2}-[a-z]{3}-20\d{2}$"
    " times
    syntax match jnlTime        "\v<(\d{1,2}((:\d{2}([ap]m)?)|([ap]m)))>"
    " durations
    syntax match jnlDuration    "\v<(\d{1,2}(.\d)?[hm]){1,2}>"
    " JIRA items
    syntax match jnlJiraItem    "\v<[A-Z]{3,}\i*-\d{1,5}>"
    " urls
    syntax match jnlUrl         "\v<(ht|f)tp(s)?://\S*>" contains=jnlJiraItem

    "highlight! link jnlLogDate   Statement
    highlight! link jnlTime      Constant
    highlight! link jnlDuration  Constant
    highlight! link jnlJiraItem  Identifier
    highlight! link jnlUrl       Special

    highlight jnlLogDate cterm=bold gui=italic guifg=#801050 ctermfg=179
endfunction

autocmd BufRead,BufEnter,BufWinEnter \d\d\d\d.txt call SetUpSyntaxColoring()

function! PrepareJournal(journal_dir)
    let l:journal_filename = strftime("%y%m.txt")
    let l:journal_path = a:journal_dir . '/' . l:journal_filename
    let l:journal_date = strftime("%d-%b-%Y")
    let l:journal_date = tolower(substitute(l:journal_date, '^0', '', ''))

    execute "cd" a:journal_dir

    " archive the txt and script files
    if has('win32')
        let l:backup_name = strftime("Dailies-%y%m%d.7z")
        let l:backup_name = a:journal_dir . '/Backups/' . l:backup_name

        call system('"c:\Program Files\7-Zip\7z.exe" a -mx=9 "' . l:backup_name . '" *.txt *.vim')
    endif

    execute "edit" l:journal_path

    " put the Vim header in if the file is new so that Vim autoformats
    " properly while typing
    "
    " The 'comments=' bit is because formatting lists doesn't work with
    " just punctuation in 7.3 due to a buggy patch. (But maybe only when
    " 'c' doesn't appear in formatoptions?)
    " see: https://groups.google.com/forum/#!topic/vim_dev/U5nknzuo5Wc
    if line('$') == 1
        let l:modeline = 'set lines=40 co=85 tw=78 fo=twan1 comments= autoindent'
        call append(0, 'vim:' . l:modeline . ':')
        execute l:modeline
    endif

    " if today's date string isn't already in the file add it
    if search(l:journal_date) == 0
        call append(line('$'), [l:journal_date, '', ''])
    endif

    normal Gzz$
endfunction

call PrepareJournal(getcwd())
