
if has("gui_macvim")
    let macvim_skip_cmd_opt_movement = 1

    "source $VIMRUNTIME/macmap.vim

    for letterindex in range(33,126)
        let letter = nr2char(letterindex)
        let letter = escape(letter, '<>|\~')
        let command = 'map <D-' . letter . '> <C-' . letter . '>'
        "echo command
        execute command
    endfor
endif

set mouse=a

set fileformats=unix,dos

set printoptions=paper:letter

" Grabbing the newly-mapped capslock key for escape.
"map  <Help> <Esc>
"map! <Help> <Esc>
"map  <Insert> <Esc>
"map! <Insert> <Esc>
nnoremap <F6> :execute '!open --reveal "' . expand("%:p") . '"'<CR><CR>
nnoremap <S-F6> :call OpenCommandPrompt()<CR>
nnoremap <F7> :call OpenInSafari(0)<CR>
vnoremap <F7> :call OpenInSafari(1)<CR>

" Alt-O becomes this character.
nnoremap ø :call EditRelatedFile()<CR>
nnoremap Ø :call SplitRelatedFile()<CR>

let s:in_insert_mode = 0

augroup MySettings
    autocmd!
    "autocmd FocusLost       ~/Documents/**/*    wa
    "autocmd FocusLost       ~/*vim*             wa
    "autocmd FocusLost       *                   call feedkeys("\<C-\>\<C-n>", "n")
    autocmd FocusLost       *                   call LeaveInsertMode()
    "autocmd BufWinEnter     *                   lcd %:p:h
    "autocmd BufRead         *.txt               call SetupTxtBuffer()
    "autocmd BufWritePost    *vimrc              source <afile>
    autocmd InsertEnter     *                   let s:in_insert_mode = 1
    autocmd InsertLeave     *                   let s:in_insert_mode = 0
augroup END

function! SetupTxtBuffer()
"    set linebreak
    setlocal noexpandtab
    setlocal textwidth=72
    setlocal autoindent
    setlocal formatoptions=tqwanb21
    setlocal nocindent
    setlocal nosmartindent
endfunction

function! LeaveInsertMode()
    "echo "v:insertmode is " . v:insertmode
    "echo "s:in_insert_mode is " . s:in_insert_mode
    if s:in_insert_mode
        call feedkeys("\<C-\>\<C-n>", "n")
        "let s:in_insert_mode = 0
    endif
endfunction

function! OpenCommandPrompt()
    let l:command = "cd " . expand("%:p:h") . "; clear"
    let l:script =
        \  "tell application \"iTerm2\"\n"
        \. "  create window with default profile\n"
        \. "  tell current session of current window\n"
        \. '    write text "' . l:command . "\"\n"
        \. "    select\n"
        \. "  end tell\n"
        \. "end tell\n"
    call system("osascript", l:script)
endfunction

" open the selection in Safari so it can be copied as rich text
function! OpenInSafari(asRange) range
    hi normal guibg=white
    if a:asRange
        let l:first = a:firstline
        let l:last = a:lastline
    else
        let l:first = 1
        let l:last = '$'
    endif
    execute l:first . "," . l:last . 'TOhtml'
    execute 'colo' g:colors_name
    silent !open -a Safari %:p
    sleep 2
    call delete(expand("%:p"))
    q!
endfunction

