" Vim syntax file
" Language: MAXScript 8.0
" Maintainer:   Tom Plunket <tom@mightysprite.com> based on work by Josh Lee <jleedev@gmail.com>
" Last Change:  3 January 2019
" Remark:   Based on MAXScript Reference 8.0
" License:  This file is placed in the public domain.
"
" Todo:
" String escapes
" Time literals
" Pathname literals
" Point literals
" Array literals
" Bitarray literals
" Name literals
" Refine the keywords: split into statements/operators
" More punctuation
" Mismatched parentheses
" A selection of built-in functions and objects

syn case ignore

" Reserved words
syn keyword msKeyword about and animate as at by case collect continue coordsys do else exit fn for function global if in local macroscript mapped max not of off on or parameters persistent plugin rcmenu return rollout set struct then to tool undo utility when where while with
syn keyword msException throw try catch
syn keyword msUnused from

" Builtins
syn keyword msBoolean true false on off
syn keyword msConstant pi e x_axis y_axis z_axis ok undefined unsupplied dontcollect
syn keyword msColor red green blue white black orange yellow brown gray
syn keyword msGlobal activeGrid ambientColor ambientColorController animationRange animButtonEnabled animButtonState autoBackup autoBackup backgroundColor backgroundColorController backgroundImageFileName cui currentMaterialLibrary displayGamma fileInGamma fileOutGamma displaySafeFrames environmentMap flyOffTime frameRate globalTracks hardwareLockID hotspotAngleSeparation keyboard lightLevel lightLevelController lightTintColor lightTintColorController listener localTime logsystem macroRecorder manipulateMode maxFileName maxFilePath maxOps meditMaterials numEffects numAtmospherics numSubObjectLevels playActiveOnly preferences realTimePlayback renderer renderDisplacements renderEffects renderHeight renderPixelAspect renderWidth rendOutputFilename rendSimplifyAreaLights rootNode rootScene sceneMaterials scriptsPath selectionSets showEndResult skipRenderedFrames sliderTime snapMode subObjectLevel sysInfo ticksPerFrame timeConfiguration timeDisplayMode toolmode trackbar trackViewNodes units useEnvironmentMap videoPostTracks viewport scanlineRender

syn keyword msDialog messageBox queryBox yesNoCancelBox

" Strings
syn region msString start=/"/ end=/"/ skip=/\\\\\|\\"/

" "Names"
syn match msName /#\i\+/

" Function args
syn match msFnArg /\i*:/

" Comments
syn region msComment start=/\/\*/ end=/\*\// contains=msTodo
syn match msComment /--.*/ contains=msTodo
syn sync ccomment msComment
set comments+=:--

syn keyword msTodo TODO FIXME NOTE XXX contained

" Literals
syn match msNumber "-\=\<\d*\.\=[0-9_]\>"

" Highlighting
hi def link msComment   Comment
hi def link msTodo      Todo

hi def link msConstant  Constant
hi def link msGlobal    Identifier
hi def link msString    String
hi def link msName      Constant
hi def link msBoolean   Boolean
hi def link msColor     Constant
hi def link msNumber    Number
hi def link msDialog    Function
hi def link msFnArg     Special

hi def link msKeyword   Keyword
hi def link msException Exception

hi def link msUnused    Error

" vim: ts=8 noet
