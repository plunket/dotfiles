" Vim syntax file

if exists("b:current_syntax")
  finish
endif
let s:keepcpo= &cpo
set cpo&vim

syn region      heatString         start='"' end='"' skip='\\"'
            \ contains=heatTodo,heatEscaped,heatLanguage,heatVariable,heatStatements,heatConditionals,heatSymCmds,heatArgument,@heatNumbers,heatSymTests
syn keyword     heatTodo contained  TODO FIXME XXX
syn match       heatEscaped         /\(\\\\\|\\"\|\\n\|\\t\)/ contained
syn match       heatVariable        /\(@[^@]*@\)\|\(\$\{1,2}\i\+\)\|\(#\{1,2}\i\+\)/

syn keyword     heatConditionals    if while for
syn match       heatConditionals    /\(&&\|||\)/
syn match       heatPounds          /^\s*#\i\+\>/
syn match       heatArgument        /\s\/\i*\(\s\|"\)/ms=s+1,me=e-1
syn keyword     heatStatements      return do add cmd new core rcmd pcmd menu3d menucmd pcmd_seq eval
syn keyword     heatSymCmds         sym sym0 sym1 symexists floatsym inc dec set setFrom setZero setNSet setOne setPair init
syn match       heatSymTests        /\([!=]=\(sym\)\?\)\|\([<>]=\?\)\|\(!\?\(isEmpty\|hasPrefix\|isOdd\|isEven\|bitSet\)\)/
" VSCode extension matched [dD][oO][lL]? also
syn keyword     heatEffects         pfx gfx maps

syn match       heatMagic           display /\(Script\|State\|StateController\|bindcmd\|[mM]aterial\|initFrom\)\s\+\i/ contains=heatIdentifier
syn match       heatInitFrom        display /\(initFrom\)\s\+\i/ contains=heatIdentifier
syn match       heatIdentifier      display / \<[a-zA-Z_][a-zA-Z0-9_-]*\>/hs=s+1 contained

syn cluster     heatLanguage        contains=heatConditionals,heatStatements,heatEffects

syn match       heatNumber          /\<\d\+\>/
syn match       heatFloat           /\<\(\d\+\.\d*\)\|\(\.\d\+\)\>/
syn cluster     heatNumbers         contains=heatNumber,heatFloat

syn match       heatLineComment     +//.*$+

syn match       heatInclude         /Include\>.*$/ contains=heatIncludeFile
syn match       heatIncludeFile     /\S*\.txt\>/ contained
"
hi def link heatPounds Special
hi def link heatConditionals Conditional
hi def link heatStatements Statement
hi def link heatEffects Special
hi def link heatMagic Type
hi def link heatInitFrom Statement
hi def link heatIdentifier Identifier
hi def link heatSymCmds Type
hi def link heatArgument Statement

hi def link heatNumber Number
hi def link heatFloat Number
hi def link heatString String
hi def link heatVariable Variable
hi def link heatSymTests Conditional

hi def link heatLineComment Comment

hi def link heatInclude Special
hi def link heatIncludeFile String

let b:current_syntax = "heat"

let &cpo = s:keepcpo
unlet s:keepcpo

" vim: set nowrap:
