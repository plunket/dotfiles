"runtime vimrc

if argc() == 0
    "lcd E:/Projects/EnvelopVR/main/sdk
    lcd E:/Projects/Immersive/main/CanyonVR/Source
    call SetUpPath(getcwd())
endif

setlocal textwidth=101
setlocal noexpandtab

nnoremap <buffer> <F3> :silent !p4 edit %<CR>:w!<CR>:e<CR>
nnoremap <buffer> <S-F3> :w<CR>:silent !p4 add %<CR>
nnoremap <buffer> <C-F3> :silent !p4 diff %<CR>
nnoremap <buffer> <S-C-F3> :silent !p4vc timelapse %<CR>

nnoremap <F8> :$tabe e:/dotfiles/vim/vimrc<CR>:sp e:/dotfiles/vim/immersive.vimrc<CR>

"iabbrev ### <C-R>=GetIncludeLine()<CR>

" remove the FixFormatting function as defined in vimrc because we don't want to strip whitespace
function! FixFormatting(filename)
    " replace spaces with tabs
    setlocal tabstop=4
    retab!
endfunction
