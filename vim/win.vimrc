" text formatting
set fileformats=dos,unix

" for whatever reason v_Ctrl-X is built-in where the other Windows keys are defined in a Windows-specific script
silent! vunmap <C-X>

nnoremap <F3> :call EditFromSourceControl(expand("%"))<CR>
nnoremap <S-F3> :call AddToSourceControl(expand("%"))<CR>
nnoremap <C-F3> :call DiffInSourceControl(expand("%"))<CR>
nnoremap <S-C-F3> :call HistoryInSourceControl(expand("%"), line('.'))<CR>

noremap <C-F4> :execute '!attrib -r "' . expand("%:p") . '"'<CR>:e<CR>
noremap <F6> :execute '!start explorer /select,"' . expand("%:p") . '"'<CR><CR>
noremap <S-F6> :call OpenCommandPrompt(expand("%:p:h"))<CR>
noremap <S-C-F6> :call OpenOnServer(expand("%:p"))<CR>
nnoremap <F7> :call OpenInIE(0)<CR>
vnoremap <F7> :call OpenInIE(1)<CR>

noremap <F12> :call OpenURL(0)<CR>
vnoremap <F12> :call OpenURL(1)<CR>

" open the selection in Internet Explorer so it can be copied as rich text
function! OpenInIE(asRange) range
    hi normal guibg=white
    if a:asRange
        let l:first = a:firstline
        let l:last = a:lastline
    else
        let l:first = 1
        let l:last = '$'
    endif
    execute l:first . "," . l:last . 'TOhtml'
    execute 'colo' g:colors_name
    silent !start "C:\Program Files (x86)\Internet Explorer\iexplore.exe" %:p
    sleep 2
    silent !del %:p
    q!
endfunction

" open the selection in Internet Explorer so it can be copied as rich text
function! OpenURL(withSelection)
    if a:withSelection
        let l:save_q = @q
        normal `<"qy`>
        let l:url = @q
        let @q = l:save_q
    else
        "normal lB"qyE
        let l:url = expand('<cfile>')
    endif
    execute 'silent !start ' . shellescape(l:url)
endfunction

function! EditFromSourceControl(filename)
    let l:scs = GetSourceControlSystem()
    if l:scs == 'p4'
        execute "silent !p4 edit " . a:filename
        write!
        edit
    endif
endfunction

function! AddToSourceControl(filename)
    let l:scs = GetSourceControlSystem()
    if l:scs == 'p4'
        write
        execute "silent !p4 add " . a:filename
    endif
endfunction

function! DiffInSourceControl(filename)
    let l:scs = GetSourceControlSystem()
    if l:scs == 'svn'
        execute "silent !start TortoiseProc /command:diff /path:" . a:filename
    elseif l:scs == 'git'
        execute "silent !start TortoiseGitProc /command:diff /path:" . a:filename
    elseif l:scs == 'p4'
        execute "silent !start p4 diff " . a:filename
    endif
endfunction

function! HistoryInSourceControl(filename, line)
    let l:scs = GetSourceControlSystem()
    if l:scs == 'svn'
        execute "silent !start TortoiseProc /command:log /path:" . a:filename
    elseif l:scs == 'git'
        "execute "silent !start TortoiseGitProc /command:log /path:" . a:filename
        execute "silent !start TortoiseGitProc /command:blame /path:" . a:filename . " /line:" . a:line
    elseif l:scs == 'p4'
        execute "silent !p4vc timelapse " . a:filename
    endif
endfunction

function! OpenCommandPrompt(directory)
    let l:scs = GetSourceControlSystem()
    if l:scs == 'git'
        echo "Launching git-bash in" a:directory
        execute 'silent !start "c:\Program Files\Git\git-bash.exe" "--cd=' . a:directory . '"'
    else
        echo "Launching cmd in" a:directory
        execute 'silent !start cmd /k cd "' . a:directory . '"'
    endif
endfunction

function! OpenOnServer(filename) range
    let l:scs = GetSourceControlSystem()
    let l:root = GetProjectRoot(a:filename)

    if l:scs == 'git'
        let l:filename = substitute(a:filename, '\\', '/', 'g')

        "let l:branch = system('git branch --show-current')
        let l:branch = 'master'
        let l:url = system('git remote get-url origin')

        " trim trailing whitespace
        let l:branch = substitute(l:branch, '\_s\+$', "", "")
        let l:url = substitute(l:url, '\_s\+$', "", "")

        if l:url =~ "^git@.*"
            let l:url = substitute(l:url, '^git@\([^:]*\):\(.*\)\.git', 'https://\1/\2', '')
        endif

        if (l:url =~ 'github') || (l:url =~ 'ghe')
            let l:url = l:url . '/tree/' . l:branch
            if a:firstline == a:lastline
                let l:anchor = printf('#L%d', a:firstline)
            else
                let l:anchor = printf('#L%d-L%d', a:firstline, a:lastline)
            endif
        elseif l:url =~ 'bitbucket'
            let l:url = l:url . '/src/' . l:branch
            if a:firstline == a:lastline
                let l:anchor = printf('#lines-%d', a:firstline)
            else
                let l:anchor = printf('#lines-%d:%d', a:firstline, a:lastline)
            endif
        endif

        "echo "silent !start " . l:url . '/' . l:filename[strlen(l:root):] . l:anchor
        execute "silent !start " . l:url . '/' . l:filename[strlen(l:root):] . '\' . l:anchor
        let @* = l:url . '/' . l:filename[strlen(l:root):] . l:anchor
    endif
endfunction
