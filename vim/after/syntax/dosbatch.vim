syn clear dosbatchInteger
syn clear dosbatchHex
syn clear dosbatchBinary
syn clear dosbatchOctal

" add + to the list of string splitters
syn match dosbatchInteger   "[[:space:]=(/:,!~+-]\d\+"lc=1
syn match dosbatchHex       "[[:space:]=(/:,!~+-]0x\x\+"lc=1
syn match dosbatchBinary    "[[:space:]=(/:,!~+-]0b[01]\+"lc=1
syn match dosbatchOctal     "[[:space:]=(/:,!~+-]0\o\+"lc=1

