setlocal textwidth=100
setlocal fileformats=unix,dos

"nnoremap <buffer> <F3> :silent !p4 edit %<CR>:w!<CR>:e<CR>
"nnoremap <buffer> <S-F3> :w<CR>:silent !p4 add %<CR>
"nnoremap <buffer> <C-F3> :silent !p4 diff %<CR>
"nnoremap <buffer> <S-C-F3> :silent !p4vc timelapse %<CR>

nnoremap <buffer> <F8> :$tabe e:/dotfiles/vim/vimrc<CR>:sp e:/dotfiles/vim/nextaudio.vimrc<CR>

setlocal colorcolumn=+1

"iabbrev ### <C-R>=GetIncludeLine()<CR>

if bufnr("$") == 1
    set columns=120
endif
