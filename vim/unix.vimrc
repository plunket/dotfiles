
set fileformats=unix,dos

if !has('gui_running')
    set mouse=n
endif

nnoremap <C-F3> :call DiffInSourceControl(expand("%"))<CR>

function! DiffInSourceControl(filename)
    let l:scs = GetSourceControlSystem()
    if l:scs == 'git'
        execute "silent !git difftool -- " . a:filename
    endif
endfunction
