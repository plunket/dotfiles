
runtime vimrc

" RAD options
let g:fixfileformatting=1
let g:radmaximizecurrentwindow=0

" list all the tags files; named _tags/tags search upward until d:/work
"set tags=_local/tags/tags;d:/work
set tags=_local/tags/tags;/
"set tags+=_local/tags/pytags;d:/work
"set tags+=_local/tags/meltags;d:/work
"set tags+=_local/tags/radtags;d:/work

if $RAD_PROJECT_DIR != ""
    " if there are no command line parameters, change into a useful directory
    " so that :find can work
    if argc() == 0
        " switch in two stages in case the current machine doesn't have a src directory
        lcd $RAD_PROJECT_DIR
        silent! lcd $RAD_PROJECT_DIR/src
    endif

    " set the path for :find, gF, etc.
    let s:rootdir = substitute($RAD_PROJECT_DIR, '^\([^/\\]*[/\\][^/\\]*\).*', '\1', '')
    "let s:path = 'src/**;' . s:rootdir . ',scripts/**;' . s:rootdir . ',sourcedb/**;' . s:rootdir . ',c:/Program\\\ Files/Autodesk/Maya2013/include,externals/**;' . s:rootdir
    let s:path = 'src/**;' . s:rootdir . ',scripts/**;' . s:rootdir . ',sourcedb/**;' . s:rootdir
    execute "set path=" . s:path

    " remove one of the extra "current dir" searches
    set path-= 
endif

if $COMPUTERNAME ==? "TOM11"
    set runtimepath^=$RAD_PROJECT_DIR\scripts\vim
    "set runtimepath^=d:\work\personal\tom\utilities\vimfiles
    set runtimepath+=$RAD_PROJECT_DIR\scripts\vim\after
else
    set runtimepath^=\\TOM11\work\rad07\dev\tool\scripts\vim
    set runtimepath^=\\TOM11\work\personal\tom\utilities\vimfiles
    set runtimepath+=\\TOM11\work\rad07\dev\tool\scripts\vim\after
endif

if $COMPUTERNAME ==? "TOM11"
    set backup      " keep a backup file
    set backupdir=~\vimbackups\
else
    set nobackup
endif

set textwidth=99
set indentkeys-=0#
set cinkeys-=0#
set complete-=i   " it takes waaay too long to scan includes with ^N and ^P

" C/C++ autoformatting
" g0 says zero spaces between encl. block and public: private: declarations default gs
" (0 says align items inside of open parens with 0 additional spaces default (2s
" (the 's' in the defaults is 'shiftwidth'; without it it would be 'chars')
" l1 says to indent after a case relative to the case statement rather than the open curly brace
" *700 says look for unclosed comments 700 lines away
set cinoptions=g0(0l1*700

augroup AutoSaveGroup
    autocmd!
    " :help file-pattern has some info,
    " but some of it is is buried in the PATTERNS section of usr_40.txt
    "autocmd FocusLost       *.{cpp,h,c,inl,rad*,r7*,py,bat,mel,msg,txt,jam,vim*}  call FocusLost()
    autocmd FocusLost       *.{rad*,r7*,msg}                                    silent! wall
    autocmd BufWritePre     *.{rad*,r7*,msg}                                    call FixFormatting()

    autocmd FileChangedRO   *.{cpp,h,c,inl,cg,rad*,r7*,py,bat,mel,msg,jam,vim*} silent !p4 edit %:p
    autocmd FileChangedRO   *.{cpp,h,c,inl,cg,rad*,r7*,py,bat,mel,msg,jam,vim*} write!

    autocmd BufWritePre     *.{cpp,h,c,inl,cg,cgfx,fx}                          call SortIncludes()
augroup END

augroup MiscellaneousTomStuff
    autocmd!
    " make up for the deficiencies in 'autochdir'
    autocmd BufEnter      *               silent! lcd %:p:h:gs/ /\\ /
    autocmd BufEnter      *               setlocal noignorecase
    "autocmd BufEnter      *.py            setlocal tags=_local/tags/pytags;d:/work
    "autocmd BufEnter      *.{rad*,r7*}    setlocal tags=_local/tags/radtags;d:/work
    "autocmd BufEnter      *.{rad*,r7*}    setlocal ignorecase
    "autocmd BufEnter      *.mel           setlocal tags=_local/tags/meltags;d:/work
    autocmd BufRead       *               setlocal textwidth=99 formatoptions=qc
    "autocmd BufRead       *.txt           setlocal textwidth=78 formatoptions=tcwanl1
    autocmd BufRead       *.txt           setlocal textwidth=0 formatoptions= colorcolumn=
    autocmd BufEnter      *.{c*,h}        setlocal comments-=:// comments+=b://\ --,://
    autocmd BufEnter      *.py            setlocal comments-=b:# comments+=b:#\ --,b:#
    autocmd BufEnter      *.bat           setlocal comments-=b:rem comments+=b:rem\ --,b:rem
    if $MYVIMRC != ''
        autocmd BufWritePost  *.vimrc   source $MYVIMRC
    endif
augroup END

" these match function open/close braces that aren't in the first column.
" since close braces are on the first column, don't need to do anything
" special for them.
" commented out versions are the ones provided by docs...
" current/previous function start
"map [[ ?{<CR>w99[{
nnoremap [[ ?{<CR>][%0:noh<CR>
" end of current/next function
"map ][ /}<CR>b99]}
"map ][ /^}<CR>0
" next function start
"map ]] j0[[%/{<CR>
nnoremap ]] k][/{<CR>0:noh<CR>
" end of previous function
" map [] k$][%?}<CR>
"map [] ?^}<CR>

nnoremap <F2> IReturnError_(<Esc>f(%a)<Esc>
nnoremap <F3> :vimgrep // $RAD_PROJECT_DIR/src/**/*.{cpp,h,c,inl,msg}<C-Left><C-Left><Right>
nnoremap <F8> :99tabe d:\work\personal\tom\utilities\vimfiles\tom.vimrc<CR>
nnoremap <F11> :call P4Add()<CR>
nnoremap <F12> :call P4Checkout()<CR>

" -- alt stuff numbers
nnoremap <A-v> :call UpdateVersionNumber()<CR>
nnoremap <A-o> :call EditRelatedFile()<CR>
nnoremap <A-O> :call SplitRelatedFile()<CR>
" -- the set cmdheight stuff is a hack to get around the fact that there is a blank line echoed
nnoremap <silent> <A-s> :set cmdheight=2 \| call PrintSyntaxItem() \| set cmdheight=1<CR>

" -- common build steps
set timeoutlen=5000
set ttimeoutlen=1000

function! SetUpCompileCommands()
    let l:prologue = 'nnoremap <LocalLeader>'
    let l:platforms = [ ['', 'win64'], ['3', 'win32'], ['p', 'ps4'] ]
    let l:compilecommands = [ 'd', 'r', 'p', 'm', 'f' ]
    let l:unittestcommands = [ ['r', 'utradattr'], ['p', 'uproperties'] ]
    for l:p in l:platforms
        execute prologue . l:p[0] . " <Nop>"
        execute prologue . l:p[0] . "u <Nop>"

        for l:c in l:compilecommands
            execute prologue . l:p[0] . l:c .
                    \" :call Compile('-', '-', 'mk" . l:c . " dist " . l:p[1] . "')<CR>"
        endfor

        for l:u in l:unittestcommands
            execute prologue . l:p[0] . 'u' . l:u[0] .
                    \" :call Compile('-', 'unittests/" . l:u[1] . "', 'mkd mt " . l:p[1] . "')<CR>"
        endfor
    endfor
endfunction

call SetUpCompileCommands()

iabbrev kNeverInedx kNeverIndex
iabbrev kNeverInedx32 kNeverIndex32
iabbrev kNeverInedx64 kNeverIndex64
iabbrev //== // <C-R>=InsertTo99Width('===')<CR><CR>//<CR>// <C-R>=InsertTo99Width('===')<CR><C-O>k
iabbrev /== // <C-R>=InsertTo99Width('===')<CR>
iabbrev //-- // <C-R>=InsertTo99Width('---')<CR><CR>//<CR>// <C-R>=InsertTo99Width('---')<CR><C-O>k
iabbrev /-- // <C-R>=InsertTo99Width('---')<CR>

"iabbrev #== # <C-R>=InsertTo99Width('===')<CR><CR>#<CR># <C-R>=InsertTo99Width('===')<CR><C-O>k
"iabbrev #-- # <C-R>=InsertTo99Width('---')<CR><CR>#<CR># <C-R>=InsertTo99Width('---')<CR><C-O>k
iabbrev #== # <C-R>=InsertTo99Width('===')<CR>
iabbrev #-- # <C-R>=InsertTo99Width('---')<CR>

iabbrev ### #include "<C-R>=expand("#:p")<CR>"<Esc>m`:s/\\/\//g<Enter>:s/\.\(cpp\\|msg\)"/.h"/e<Enter>:s/".*\/src\/[^\/]*\//"/<Enter>:noh<Enter>``a

iabbrev === <C-R>=InsertTo99Width('===')<CR>
iabbrev --- <C-R>=InsertTo99Width('---')<CR>

nnoremap <C-Tab> :silent exec '!start "c:\Program Files (x86)\Microsoft Visual Studio 10.0\Common7\IDE\devenv.exe" /Edit "' . expand("%:p") . '"'<CR>


function! LocalFixFormatting(filename)
    " -- deal with the block separators.  Assume that any line that has only equals or dashes past
    " -- column 92 is a candidate line

    " extend separator bars to 99 columns
    silent! global/^.\{40,92}======$/execute "normal A\<C-R>=InsertToTextwidth('=')\<Enter>"
    silent! global/^.\{40,92}------$/execute "normal A\<C-R>=InsertToTextwidth('-')\<Enter>"

    " truncate any separators that go past column 99
    %s/^\(.\{92}[=-]\{7}\)[=-]\+/\1/e
endfunction

function! SortIncludes()
    let l:winview = winsaveview()
    call cursor(1, 1)

    let l:done = 0

    " -- find the next line that starts with #include
    while !l:done && search('^\(\s\|//\)*#include "', 'W')
        let l:startline=line('.')
        " -- and the next line that doesn't...
        let l:endline=search('^\(\(\s\|//\)*\)\@>\(#include\)\@!')
        if l:endline < l:startline
            let l:endline = line('$')
            let l:done = 1
        endif
        let l:lines=getline(l:startline, l:endline-1)
        let l:origlines=copy(l:lines)
        if 0
            echo 'start: ' . l:startline . ', end: ' . l:endline
            echo l:origlines
            echo l:lines
        endif
        call sort(l:lines, 1)
        if l:origlines != l:lines
            call setline(l:startline, l:lines)
        endif
        call cursor(endline, 1)
    endwhile

    call winrestview(l:winview)
endfunction

"function! RadattrDumpFoldLevelLine(lnum)
"    let line = getline(a:lnum)
"    let prevline = getline(a:lnum - 1)
"    let nextline = getline(a:lnum + 1)
"
"    let indent = (matchend(line, '^\s*[+*] ') - 2) / 2
"    let previndent = (matchend(prevline, '^\s*[+*] ') - 2) / 2
"    let nextindent = (matchend(nextline, '^\s*[+*] ') - 2) / 2
"
""    echo 'p:' . previndent . ' =>' . indent . ' n:' . nextindent
"
"    if indent < 0
"        return "0"
"    elseif nextindent > indent
"        return '>'.(indent + 1)
"    elseif previndent > indent
"        return '>'.(indent + 1)
"    else
"        return '='
"    endif
"endfunction
"
"function! RadattrDumpFoldLevel()
"    return RadattrDumpFoldLevelLine(v:lnum)
"endfunction
"
"function! RadattrDumpFoldText()
"    return getline(v:foldstart) . ' (' . (v:foldend - v:foldstart + 1) . ' lines)'
"endfunction
"
"function! FoldRadattrDump()
"    setlocal nowrap             " usually want this off when dealing with radattr dumps
"    if 0
"        setlocal shiftwidth=2       " each indent is two spaces then a plus or an asterisk
"        setlocal foldmethod=indent  " fold based on indent level
"    else
"        setlocal foldmethod=expr  " fold based on the following expression
"        setlocal foldexpr=RadattrDumpFoldLevel()
"    endif
"    setlocal foldtext=RadattrDumpFoldText()
"endfunction

function! PrintSyntaxItem()
    let l:colorsyntax = synIDattr(synID(line("."), col("."), 0), "name")
    execute "highlight" l:colorsyntax
endfunction

