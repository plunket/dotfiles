runtime vimrc

" The initial nvim config is in $XDG_CONFIG_HOME/nvim/init.vim
" On Windows the path is %LOCALAPPDATA%\nvim\init.vim
" Create a file there that 'runtime's this one.
" Manually select the contents of the if 0 then source it.
if 0
function! SetUpNewSystem()
    let l:vimfiles_path = fnamemodify(expand("%"), ":p:h")
    let l:vimfiles_path = substitute(l:vimfiles_path, '\', '/', 'g')
    let l:config_path = '~/.config'
    if exists("$XDG_CONFIG_HOME") && isdirectory($XDG_CONFIG_HOME)
        let l:config_path = $XDG_CONFIG_HOME
    elseif exists("$LOCALAPPDATA") && isdirectory($LOCALAPPDATA)
        let l:config_path = $LOCALAPPDATA
    endif
    let l:init_path = expand(l:config_path . '/nvim/init.vim')
    call mkdir(fnamemodify(l:init_path, ":p:h"), "p")
    execute "split" l:init_path
    $append
set runtimepath^=@@@@
set runtimepath+=@@@@/after
set runtimepath-=$LOCALAPPDATA\\nvim
set runtimepath-=$LOCALAPPDATA\\nvim\\after
set runtimepath-=$LOCALAPPDATA\\nvim-data\\site\\after
set runtimepath-=$LOCALAPPDATA\\nvim-data\\site
runtime init.vim
.
    normal 4k
    execute '.,+1s;@@@@;' . l:vimfiles_path . ';'
endfunction
call SetUpNewSystem()
endif

if exists("g:neovide")
    set guifont=Consolas:h12
    set lines=60
    set columns=100
    "echo &lines &columns
endif

if exists("g:vscode")
    "echo "VSCode extension"
    "local vscode = require('vscode')
    "execute "nnoremap <F8> :Edit " . fnamemodify(expand('<sfile>'), ':gs?\\?\/?') . "<CR>"
else
    "echo "Ordinary Neovim"
    execute "nnoremap <F8> :$tabedit " . expand('<sfile>') . "<CR>"
endif

if exists("g:neovide")
    function! PrintNeovideGlobals()
        let l:varnames = [
            \ "position_animation_length    ",
            \ "cursor_animation_length      ",
            \ "cursor_trail_size            ",
            \ "cursor_animate_in_insert_mode",
            \ "cursor_animate_command_line  ",
            \ "scroll_animation_far_lines   ",
            \ "scroll_animation_length      "
            \ ]
        for l:vn in l:varnames
            execute "echo '" . vn . " =' g:neovide_" . vn
        endfor
    endfunction
    "call PrintNeovideGlobals()
    let g:neovide_position_animation_length=0.05
    let g:neovide_cursor_animation_length=0.05
    let g:neovide_cursor_trail_size=0.2
    let g:neovide_cursor_animate_in_insert_mode=v:false
    let g:neovide_cursor_animate_command_line=v:false
    let g:neovide_scroll_animation_far_lines=10
    let g:neovide_scroll_animation_length=0.1

    let g:neovide_remember_window_position = v:true || v:false
    let g:neovide_remember_window_size = v:true || v:false

    "augroup TomNeovide
    "    autocmd!
    "    autocmd VimResized * let &columns=&columns+1
    "    autocmd VimResized * let &columns=&columns+1
    "    autocmd VimResized * echo "Let's go!"
    "augroup END

endif

silent! unmap Y
noremap <C-W><C--> <C-W><C-_>
