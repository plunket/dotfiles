let g:fix_formatting_coalesce_blank_lines=0
let g:fix_formatting_retab=0
let g:update_copyright_namematch = '([pP]lunket|Carbon|Anduril)'

runtime vimrc

set noexpandtab
set backup
set textwidth=120

"   #s don't force the #if etc to the left column
set cinoptions+=#+s
"   N-s back-dent stuff inside namespace scopes
set cinoptions-=N-s

call AddToF8(expand("<sfile>"), "")

function! LocalFixFormatting(filename)
    call UpdateCopyrightNotice()
    if search('Copyright.*\D202\d\D.*Carbon Games', "w")
        substitute+Air[mM]ech+Baywatch+e
        substitute+Carbon Games+Anduril Industries+e
    endif
    if a:filename !~ '^\d\{4}\.txt$'
        let l:lastline = line('$')
        let l:curline = line('.')
        while l:curline != l:lastline && getline(l:curline) =~ '^\s*$'
            let l:curline = l:curline + 1
        endwhile
        if l:curline != l:lastline
            while getline(l:lastline) =~ '^\s*$'
                execute l:lastline . "delete"
                let l:lastline = line('$')
            endwhile
        endif
    endif
endfunction

function! GenerateSkeleton(filename)
    let l:extension = fnamemodify(a:filename, ":e")
    if l:extension =~ 'c\(pp\|c\)\?$'
        execute 'normal O#include "heatEngineConfig.h"'
        execute 'normal o#include "' . fnamemodify(a:filename, ":r") . '.h"'
    elseif l:extension =~ 'h\(pp\|h\)\?$'
        normal O#pragma once
    endif
    normal Go
endfunction

function! SetUpPath(filepath)
    let l:pathRoot = fnameescape(GetProjectRoot(a:filepath))

    if isdirectory(l:pathRoot . 'BWVR')
        let l:pathCmd = 'setlocal path=' . l:pathRoot . 'BWVR/src/**,' . l:pathRoot . 'HeatEngine/**,' . l:pathRoot . 'BWVR/scripts/**,' . l:pathRoot . 'BWVR/data/gameScripts/**'
    else
        let l:pathCmd = 'setlocal path=./**,' . l:pathRoot . '**'
    endif
    "echo l:pathCmd
    execute l:pathCmd
endfunction

function! BuildBaywatch(project, platform, target, mechanism)
    let l:branch = GetProjectRoot(expand('%:p'))
    let l:cwd = getcwd()

    "call EnsurePreviewWindow()
    execute "lcd" l:branch
    execute "make" a:project.a:target a:platform a:mechanism

    execute "cd" l:cwd
endfunction

function! SetUpBaywatchBuildCommands()
    let l:prologue = 'nnoremap <LocalLeader>'
    let l:mechanisms = [ ['', ''], ['r', 'Rebuild'] ]
    "let l:projects = [ ['b', 'BWVR'], ['c', 'AllForOne'], ['f', 'Factories'],
    "                \ ['w', 'Wastelands'], ['a', 'Command'], ['s', 'Strike'] ]
    let l:projects = [ ['b', 'BWVR'] ]
    let l:platforms = [ ['o', 'oculusvr'], ['w', 'win64'] ]
    let l:targets = [ ['d', 'Debug'], ['r', 'Release'], ['t', 'Retail'] ]

    execute prologue . 'r <Nop>'

    for l:m in l:mechanisms
        for l:pr in l:projects
            execute prologue . l:m[0] . l:pr[0] . " <Nop>"

            for l:pl in l:platforms
                execute prologue . l:m[0] . l:pr[0] . l:pl[0] . " <Nop>"

                for l:t in l:targets
                    execute prologue . l:m[0] . l:pr[0] . l:pl[0] . l:t[0]
                        \" :call BuildBaywatch('" . l:pr[1] . "', '" . l:pl[1] . "', '" . l:t[1] . "', '" . l:m[1] . "')<CR><CR>"
                endfor
            endfor
        endfor
    endfor

    let &makeprg = "build_one_2019.bat"
    let &errorformat = '%*\s%*\d>%f(%l\,%c): %t%*\D%n:%m [%*\f]'
                   \. ',%*\s%*\d>%f(%l): %t%*\D%n:%m [%*\f]'
                   \. ',%*\s%f(%l\,%c): message : %m [%*\f]'
                   \. ',%*\s%f(%l): message : %m [%*\f]'
endfunction

function! BuildGo(filename, command)
    let l:path = fnamemodify(a:filename, ":p:h")

    let l:cwd = getcwd()

    "call EnsurePreviewWindow()
    execute "lcd" l:path
    execute "make" a:command "."
    execute "cd" l:cwd
endfunction

function! SetUpGoBuildCommands()
    let l:prologue = 'nnoremap <LocalLeader>g'
    let l:commands = [ ['b', 'build'], ['t', 'test'], ['f', 'fmt'] ]
    execute prologue . ' <Nop>'
    for l:c in l:commands
        execute l:prologue . l:c[0] . ' :call BuildGo(expand("%"), ' . "'" . l:c[1] . "')<CR><CR>"
    endfor

    let &makeprg = "go"

    " The compile errors
    let &errorformat =
        \   '%f:%l:%c: %m,'
        \ . '%*\s%m %f:%l:%c,'
        \ . '%-G# %s,'
        \ . '%-GFAIL%*\s%f%*\s[%m],'
        \ . '%-GFAIL,'

    " The testing errors
    let &errorformat .= ''
        "\ .  '%+G--- FAIL:%.%#,'
        \ .     '%E    %f:%l:%*\s,'
        \ .     '%C%*\sError Trace:%.%#,'
        \ .     '%C%*\sError:%m,'
        \ .     '%C%*\sTest:%.%#,'
        \ .     '%C%*\sMessages:%m'
endfunction

function! BuildPython(filename)
    let l:branch = GetProjectRoot(expand('%:p'))
    let l:cwd = getcwd()

    "call EnsurePreviewWindow()
    execute "lcd" l:branch
    if a:filename != ''
        execute "make" a:filename
    else
        make
    endif

    execute "cd" l:cwd
endfunction

function! SetUpPythonBuildCommands()
    nnoremap <LocalLeader>pt :call BuildPython('')<CR><CR>
    nnoremap <LocalLeader>pf :call BuildPython(expand('%'))<CR><CR>
    let &makeprg = "pytest --tb=short -vv"

    let &errorformat =
       \   '%EE     File "%f"\, line %l,'
       \ . '%CE   %p^,'
       \ . '%ZE   %[%^ ]%\@=%m,'
       \ . '%Afile %f\, line %l,'
       \ . '%+ZE %mnot found,'
       \ . '%CE %.%#,'
       \ . '%-G_%\+ ERROR%.%# _%\+,'
       \ . '%A_%\+ %o _%\+,'
       \ . '%C%f:%l: in %o,'
       \ . '%ZE %\{3}%m,'
       \ . "%EImportError%.%#'%f'.,"
       \ . '%C%.%#,'
       \ . '%+G%[=]%\+ %*\d passed%.%#,'
       \ . '%-G%[%^E]%.%#,'
       \ . '%-G'
endfunction

function! SetUpBuildCommands(filename)
    let l:rootDir = GetProjectRoot(a:filename)
    let l:didSetup = 1
    if filereadable(l:rootDir . 'build_one_2019.bat')
        call SetUpBaywatchBuildCommands()
    elseif filereadable(l:rootDir . 'go.mod')
        call SetUpGoBuildCommands()
    elseif filereadable(l:rootDir.'requirements.txt') || filereadable(l:rootDir.'setup.py') || filereadable(l:rootDir.'pyproject.toml')
        call SetUpPythonBuildCommands()
    else
        let l:didSetup = 0
    endif

    if l:didSetup
        augroup DeferredBuildSetup
            autocmd!
        augroup END
    endif
endfunction

function! RenumberProtoMessage()
    execute "normal Vi{\<Esc>"
    let l:firstline = getpos("'<")[1]
    let l:lastline = getpos("'>")[1]
    let l:curline = l:firstline
    let l:counter = 1
    if match(getline(l:curline-1), '^\s*enum') >= 0
        let l:counter = 0
    endif
    while l:curline <= l:lastline
        let l:line = getline(l:curline)
        if match(l:line, '=\s*\d\+\s*\(;\)\?') >= 0
            let l:line = substitute(l:line, '=\s*\d\+\s*\(;\)\?', '= ' . l:counter . ';', '')
            call setline(l:curline, l:line)
            let l:counter = l:counter + 1
        elseif match(l:line, '\i\+\s*\(=\s*\)\?;') >= 0
            let l:line = substitute(l:line, '\s*\(=\s*\)\?;', ' = ' . l:counter . ';', '')
            call setline(l:curline, l:line)
            let l:counter = l:counter + 1
        elseif match(l:line, '^\s*\(message\|enum\)\s\+') >= 0
            call setpos('.', [0, l:curline, 0 ,0])
            normal $%
            let l:nextline = getpos('.')[1]
            call setpos('.', [0, l:curline + 1, 0 ,0])
            call RenumberProtoMessage()
            let l:curline = l:nextline
        endif
        let l:curline = l:curline + 1
    endwhile
endfunction

augroup DeferredBuildSetup
    autocmd!
    autocmd BufRead * call SetUpBuildCommands(''.expand("<afile>"))
augroup END

augroup QfMake
    autocmd!
    autocmd QuickFixCmdPost * execute "cwindow" (&lines / 3)
augroup END

augroup AndurilApis
    autocmd!
    autocmd BufRead,BufNewFile *.proto  setlocal tabstop=2 expandtab shiftwidth=2 cindent
augroup END

augroup AutoSaveGroup
    autocmd BufWritePre *.go call SortGoIncludes()
augroup END
