runtime gvimrc

if has("linux")
    if has("nvim")
        "set guifont=Consolas:h14
    else
        set guifont=Liberation\ Mono\ 14
    endif
endif
