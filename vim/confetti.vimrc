"runtime vimrc

"if argc() == 0
"   lcd E:/Projects/Confetti/starvr-sdk
"   call SetUpPath(getcwd())
"endif

setlocal textwidth=101
setlocal noexpandtab
setlocal shiftwidth=4 " make sure to reset sw and ts in case some file changed it on us.
setlocal tabstop=4
setlocal cinoptions=:0g0(0l1j0*700s+s

if !exists("s:did_widen")
    let s:did_widen = 1
    if &columns < &textwidth
        let &columns = &textwidth
    endif
endif

"iabbrev ### <C-R>=GetIncludeLine()<CR>

" remove the FixFormatting function as defined in vimrc because we don't want to strip whitespace
function! FixFormatting(filename)
    if 0
        let l:winview = winsaveview()

        " strip trailing whitespace
        let l:previousSearch = @/
        %s/\s\+$//e

        " coalesce multiple blank lines into one.
        %s/\n\n\(\n\)\+/\r\r/e
        let @/ = l:previousSearch
    endif

    " replace spaces with tabs
    setlocal tabstop=4
    "retab!
endfunction

function! StripRootPath(fullPath)
    let l:normPath = substitute(a:fullPath, '/', '\', 'g')
    return substitute(l:normPath, '^.*\\starvr-sdk\\', '', '')
endfunction

function! GetRelatedFileList()
    " Multiple :r's makes all extensions get stripped.
    "let l:path = StripRootPath(expand("%:p:r:r:r:r:r:r:r:r") . '.*')
    let l:path = expand("%:p:r:r:r:r:r:r:r:r") . '.*'
    let l:dirs = split(l:path, '\\')

    let l:thisPath = join(l:dirs, '\')
    let l:files = glob(l:thisPath)
    let l:localList = split(l:files, '[\r\n]\+')

    let l:sdk = 0
    while (l:sdk < len(l:dirs)) && (l:dirs[l:sdk] !~# '^starvr-sdk')
        let l:sdk = l:sdk + 1
    endwhile

    let l:remoteList = []
    if (l:sdk < len(l:dirs))
        let l:tweakedDirs = []
        if (l:dirs[l:sdk+1] ==? 'include')
            let l:tweakedDirs = l:dirs[:l:sdk] + ['StarVR', 'src'] + l:dirs[l:sdk+2:]
        elseif (l:dirs[l:sdk+1] ==? 'StarVR') && (l:dirs[l:sdk+2] == 'src')
            let l:tweakedDirs = l:dirs[:l:sdk] + ['include'] + l:dirs[l:sdk+3:]
        endif

        if len(l:tweakedDirs) > 0
            let l:tweakedPath = join(l:tweakedDirs, '\')
            let l:files = glob(l:tweakedPath)
            let l:remoteList = split(l:files, '[\r\n]\+')
        endif
    endif

    return l:localList + l:remoteList
endfunction

function! GetAlternateFileHeaderName(extension)
    let l:altfile = expand("#:p")
    let l:altfile = StripRootPath(l:altfile)

    let l:headerpath = fnamemodify(l:altfile, ':r') . '.h'
    let l:headerpath = substitute(l:headerpath, '^StarVR\\', '', '')
    let l:headerpath = substitute(l:headerpath, '^Framework\\', '', '')

    return l:headerpath
endfunction
