"let s:started = 0
"if (!s:started) && (argc() == 0)
"    "execute "lcd" expand("<sfile>:p:h")
"    "call SetUpPath(getcwd())
"    call SetUpPath(expand("<sfile>:p:h"))
"endif
"let s:started = 1

setlocal textwidth=101
setlocal expandtab

"iabbrev ### <C-R>=GetIncludeLine()<CR>

let g:fix_formatting_delete_trailing_spaces = 1
let g:fix_formatting_coalesce_blank_lines = 0
let g:fix_formatting_retab = 0
