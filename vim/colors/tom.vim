" Vim color file
" Maintainer:
" Last Change:
" Last Change:
" URL:
" Version:

" cool help screens
" :he group-name
" :he highlight-groups
" :he cterm-colors

if has("gui_running")
    set background=light
else
    set background=dark
endif

if version > 580
    " no guarantees for version 5.8 and below, but this makes it stop
    " complaining
    hi clear
    if exists("syntax_on")
    syntax reset
    endif
endif
let g:colors_name="tom"

if has("gui_running")
    "hi Normal   guifg=gray10 guibg=#f8f6f4
    "hi Normal   guifg=gray10 guibg=#f8e8dc
    "hi Normal   guifg=gray10 guibg=#f8eae6
    hi Normal   guifg=gray10 guibg=#f8f0e0

    " highlight groups
    ""hi Cursor   guibg=khaki guifg=slategrey
    ""hi CursorIM
    ""hi Directory
    ""hi DiffAdd
    ""hi DiffChange
    ""hi DiffDelete
    ""hi DiffText
    ""hi ErrorMsg
    "hi VertSplit    guibg=#c2bfa5 guifg=grey50 gui=none
    "hi Folded   guibg=grey30 guifg=gold
    "hi FoldColumn   guibg=grey30 guifg=tan
    "hi IncSearch    guifg=slategrey guibg=khaki
    ""hi LineNr
    "hi ModeMsg  guifg=goldenrod
    "hi MoreMsg  guifg=SeaGreen
    "hi NonText  guifg=LightBlue guibg=grey30
    "hi Question guifg=springgreen
    "hi Search   guibg=peru guifg=wheat
    "hi SpecialKey   guifg=yellowgreen
    "hi StatusLine   guibg=#c2bfa5 guifg=black gui=none
    "hi StatusLineNC guibg=#c2bfa5 guifg=grey50 gui=none
    "hi Title    guifg=indianred
    "hi Visual   gui=none guifg=khaki guibg=olivedrab
    ""hi VisualNOS
    "hi WarningMsg   guifg=salmon
    ""hi WildMenu
    ""hi Menu
    ""hi Scrollbar
    ""hi Tooltip

    "
    "" syntax highlighting groups
    hi Comment  guifg=DarkOliveGreen
    hi Constant guifg=DarkMagenta
    "hi Identifier   guifg=palegreen
    "hi Statement    guifg=orangered4
    hi Statement    gui=NONE guifg=#803000  " darker than darkgoldenrod
    "hi Statement    guifg=PaleVioletRed4
    hi PreProc  guifg=SlateBlue4
    hi Type     gui=NONE guifg=RoyalBlue3
    "hi Special  guifg=DarkSlateBlue
    hi Special  guifg=DarkSlateBlue
    hi SpecialKey guifg=BlueViolet
    "hi StatusLine   guibg=DarkOliveGreen1
    hi StatusLine   term=bold,reverse cterm=bold,reverse gui=bold guifg=DarkOliveGreen1 guibg=Black
    hi StatusLineNC term=bold,reverse cterm=bold,reverse gui=none guifg=#f8f0e0 guibg=Black
    "hi NonText  guibg=#e4e0dc guifg=BlueViolet
    hi NonText  guifg=Magenta3
    hi ColorColumn guibg=#f6e8d8
    "hi ColorColumn gui=reverse
    "NonText        xxx term=bold gui=bold guifg=Blue
    ""hi Underlined
    "hi Ignore   guifg=grey40
    ""hi Error
    "hi Todo     guifg=orangered guibg=yellow2
    "
    "" color terminal definitions
elseif &t_Co==256
    " https://imgur.com/okBgrw4
    hi Normal       ctermbg=235
    hi Comment      ctermfg=108
    hi Statement    ctermfg=179
    hi Type         ctermfg=123
    hi IncSearch    cterm=NONE ctermfg=0 ctermbg=228
    hi Search       cterm=NONE ctermfg=0 ctermbg=69
elseif &t_Co==16
    hi Normal   ctermfg=LightGray ctermbg=Black
    hi Folded  ctermbg=black ctermfg=darkgray

    hi Comment  ctermfg=DarkGreen
    hi Constant ctermfg=Cyan
    hi Identifier cterm=NONE ctermfg=White
    hi PreProc  ctermbg=DarkBlue ctermfg=Cyan
    "hi Special  ctermbg=DarkBlue ctermfg=LightRed
    hi Special  ctermfg=LightGreen
    hi SpecialKey ctermbg=DarkBlue ctermfg=Yellow
    "hi StatusLine   ctermbg=DarkBlue ctermfg=Cyan
    "hi StatusLineNC   ctermbg=DarkBlue ctermfg=DarkGray
    hi StatusLine   ctermbg=DarkBlue ctermfg=Yellow
    hi StatusLineNC   ctermbg=DarkBlue ctermfg=DarkGray
    "hi NonText  ctermbg=DarkGreen ctermfg=Blue
    hi NonText  ctermfg=LightMagenta
    hi ColorColumn ctermbg=DarkRed
    "hi ColorColumn cterm=reverse

    "hi SpecialKey   ctermfg=darkgreen
    "hi NonText  cterm=NONE ctermfg=darkblue
    hi Directory    ctermfg=darkcyan
    "hi ErrorMsg cterm=NONE ctermfg=7 ctermbg=1
    hi IncSearch    cterm=NONE ctermfg=yellow ctermbg=green
    "hi IncSearch cterm=NONE ctermfg=blue ctermbg=yellow
    "hi Search   cterm=NONE ctermfg=grey ctermbg=blue
    hi Search cterm=NONE ctermfg=green ctermbg=blue
    hi MoreMsg  cterm=NONE ctermfg=brown
    hi ModeMsg  cterm=NONE ctermfg=brown
    "hi LineNr   ctermfg=3
    "hi Question ctermfg=green
    "hi StatusLine   cterm=reverse
    "hi StatusLineNC cterm=reverse
    "hi VertSplit    cterm=reverse
    "hi Title    ctermfg=5
    hi Visual   cterm=reverse
    "hi VisualNOS    cterm=underline
    "hi WarningMsg   ctermfg=1
    "hi WildMenu ctermfg=0 ctermbg=3
    "hi Folded   ctermfg=darkgrey ctermbg=NONE
    "hi FoldColumn   ctermfg=darkgrey ctermbg=NONE
    "hi DiffAdd  ctermbg=4
    "hi DiffChange   ctermbg=5
    "hi DiffDelete   cterm=NONE ctermfg=4 ctermbg=6
    "hi DiffText cterm=NONE ctermbg=1
    "hi Comment  ctermfg=darkcyan
    "hi Constant ctermfg=brown
    "hi Special  ctermfg=5
    "hi Identifier   ctermfg=6
    "hi Statement    ctermfg=3
    "hi PreProc  ctermfg=5
    "hi Type     ctermfg=2
    "hi Underlined   cterm=underline ctermfg=5
    "hi Ignore   cterm=NONE ctermfg=7
    "hi Ignore   ctermfg=darkgrey
    "hi Error    cterm=NONE ctermfg=7 ctermbg=1

    " move the console stuff here.
    hi MatchParen term=reverse ctermbg=5
    hi Cursor term=reverse ctermbg=3 ctermfg=0
endif

"vim: sw=4
