set guioptions-=tT   " turn off toolbar
"set guioptions-=m    " turn off menu
set guioptions+=c    " console dialogs instead of popups

"set guifont=DejaVu\ Sans\ Mono:h13
set guifont=Monaco:h13

  "macm File.New\ Window				action=newWindow: key=<D-n> 

" the following lifted from /Applications/MacVim.app/Contents/Resources/vim/runtime/menu.vim
"
" Set up menu key equivalents (these should always have the 'D' modifier
" set), action bindings, and alternate items.
"
" Note: menu items which should execute an action are bound to <Nop>; the
" action message is specified here via the :macmenu command.
"
"macm File.New\ Window				key=<nop> action=newWindow:
"macm File.New\ Tab				key=<nop>
"macm File.Open\.\.\.				key=<nop> action=fileOpen:
"macm File.Open\ Tab\.\.\.<Tab>:tabnew		key=<nop>
"macm File.Open\ Recent			action=recentFilesDummy:
"macm File.Close\ Window<Tab>:qa		key=<nop>
"macm File.Close				key=<nop> action=performClose:
"macm File.Save<Tab>:w				key=<nop>
"macm File.Save\ All				key=<nop> alt=YES
"macm File.Save\ As\.\.\.<Tab>:sav		key=<nop>
"macm File.Print				key=<nop>
"
"macm Edit.Undo<Tab>u				key=<nop> action=undo:
"macm Edit.Redo<Tab>^R				key=<nop> action=redo:
"macm Edit.Cut<Tab>"+x				key=<nop> action=cut:
"macm Edit.Copy<Tab>"+y			key=<nop> action=copy:
"macm Edit.Paste<Tab>"+gP			key=<nop> action=paste:
"macm Edit.Select\ All<Tab>ggVG		key=<nop> action=selectAll:
"macm Edit.Find.Find\.\.\.			key=<nop>
"macm Edit.Find.Find\ Next			key=<nop> action=findNext:
"macm Edit.Find.Find\ Previous			key=<nop> action=findPrevious:
"macm Edit.Find.Use\ Selection\ for\ Find	key=<nop>
"macm Edit.Font.Show\ Fonts			action=orderFrontFontPanel:
"macm Edit.Font.Bigger				key=<nop> action=fontSizeUp:
"macm Edit.Font.Smaller			key=<nop> action=fontSizeDown:
"macm Edit.Special\ Characters\.\.\.		key=<nop> action=orderFrontCharacterPalette:
"
"macm Tools.Spelling.To\ Next\ error<Tab>]s	key=<nop>
"macm Tools.Spelling.Suggest\ Corrections<Tab>z=   key=<nop>
"macm Tools.Make<Tab>:make			key=<nop>
"macm Tools.List\ Errors<Tab>:cl		key=<nop>
"macm Tools.Next\ Error<Tab>:cn		key=<nop>
"macm Tools.Previous\ Error<Tab>:cp		key=<nop>
"macm Tools.Older\ List<Tab>:cold		key=<nop>
"macm Tools.Newer\ List<Tab>:cnew		key=<nop>
"
"macm Window.Minimize		key=<nop>	action=performMiniaturize:
"macm Window.Minimize\ All	key=<nop>	action=miniaturizeAll:	alt=YES
"macm Window.Zoom		key=<nop>	action=performZoom:
"macm Window.Zoom\ All		key=<nop>	action=zoomAll:		alt=YES
"macm Window.Toggle\ Full\ Screen\ Mode	key=<nop>
"macm Window.Select\ Next\ Tab			key=<nop>
"macm Window.Select\ Previous\ Tab		key=<nop>
"macm Window.Bring\ All\ To\ Front		action=arrangeInFront:
"
"macm Help.MacVim\ Help			key=<nop>
"macm Help.MacVim\ Website			action=openWebsite:
