"set guifont=Lucida_Console:h12:cANSI
"set guifont=Courier_New:h9:cANSI
"set guifont=Consolas:h9:cANSI
set guifont=Consolas:h11
set guifontwide=MS\ Gothic

set guioptions=cer

if has("directx")
    set renderoptions=type:directx
endif
