" This can just be dropped into .vim/ftplugin and any time you have a JSON file
" (or :setf json on pasted content) they can be called.

let g:json_reformat_singleline_block_length_default = 30

function! CountPrecedingBackslashes()
    let l:count = 0
    let l:previousChar = strpart(getline("."), col(".")-2, 1)
    while l:previousChar == '\'
        let l:count = l:count + 1
        let l:previousChar = strpart(getline("."), col(".")-(2+l:count), 1)
    endwhile
    return l:count
endfunction

function! FormatJsonBuffer(max_inline_block_length = g:json_reformat_singleline_block_length_default)
    let l:block_length = a:max_inline_block_length
    if l:block_length <= 0
        " don't turn {} and [] into {\n\n} and [\n\n]
        let l:block_length = 1
    endif

    normal gg0
    let l:keepGoing = 1
    while l:keepGoing
        let l:currentChar = strpart(getline("."), col(".")-1, 1)

        " flags to search():
        "  c - accept match at cursor position
        "  z - start searching at cursor position instead of column zero
        "  W - don't wrap
        let l:nextFlags = 'czW'

        if l:currentChar == '"'
            normal f"
            while (CountPrecedingBackslashes() % 2) == 1
                normal f"
            endwhile
            let l:nextFlags = 'zW'
        elseif l:currentChar == ''''
            normal f'
            let l:nextFlags = 'zW'
        elseif l:currentChar == ','
            execute "normal a\<Enter>"
        elseif l:currentChar =~ '[{[]'
            let l:currentLocation = col(".")
            normal %
            if col(".") - l:currentLocation > l:block_length
                "execute "normal %a\<Enter>\<Esc>k$%i\<Enter>\<Esc>%j0"
                execute "normal i\<Enter>\<Esc>%a\<Enter>\<Esc>"
                redraw
            endif
        "elseif l:currentChar == '}'
        "    execute "normal i\<Enter>\<Esc>l"
        "    let l:nextFlags = 'zW'
        elseif l:currentChar =~ '[}\]]'
            let l:nextChar = strpart(getline("."), col("."), 1)
            if l:nextChar == ","
                execute "normal la\<Enter>"
            else
                normal l
                let l:nextFlags = 'zW'
            endif
		else
			echo "FormatJsonBuffer stuck"
        endif

        let l:keepGoing = search('[{},\[\]"'']', l:nextFlags)
		"echo l:nextFlags l:keepGoing line(".") col(".")
    endwhile
endfunction

function! DeformatJsonBuffer()
    %s/^\s*//
    normal ggVGgJ
endfunction

function! StripFalsey()
    try
        while 1
            silent %s/"[^"]*":\_s*\(null\|""\|0\|false\|\[\_s*]\|{\_s*}\)\(,\)\?//g
        endwhile
    catch /.*/
        silent! g/^\s*$/d
        silent %s/,\_s\{-}\ze\(\n\s*\)\?[\]}]//ge
    endtry
endfunction
