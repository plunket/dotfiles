autocmd BufRead,BufNewFile *.txt call DetectHeatFile(expand("<afile>:p"), expand("<abuf>"))
autocmd BufRead,BufNewFile *.{mtl,mmt,mtc,lvl} set filetype=heat formatoptions=

function DetectHeatFile(filename, bufnum)
    let l:winview = winsaveview()
    call cursor(1, 1)
    if (search('\v^\s*(State(Controller)?|unitType)\s+\i+\s*(\{\s*)$', "", 5) > 0) ||
     \ (search('\v^\s*(//\s*)?(\d+\s+)?(([rp]|menu)?cmd)|(sym\s+(set|init))|defaultExpanded>', "", 100) > 0) ||
     \ (search('\v^\s*add\s*\i+\s*initFrom\s*\i+', "", 100) > 0)
        set filetype=heat
        set formatoptions=
    endif
    call winrestview(l:winview)
endfunction
