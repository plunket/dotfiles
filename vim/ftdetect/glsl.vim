" Language: OpenGL Shading Language
" Maintainer: Sergey Tikhomirov <sergey@tikhomirov.io>

autocmd! BufNewFile,BufRead *.{glsl,geom,vert,frag,gsh,vsh,fsh,vs,fs,shd,metal,sc} set filetype=glsl

" vim:set sts=2 sw=2 :
