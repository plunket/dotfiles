let s:headers = [
    \ 'algorithm',
    \ 'allocators',
    \ 'array',
    \ 'atomic',
    \ 'bitset',
    \ 'cassert',
    \ 'ccomplex',
    \ 'cctype',
    \ 'cerrno',
    \ 'cfenv',
    \ 'cfloat',
    \ 'chrono',
    \ 'cinttypes',
    \ 'ciso646',
    \ 'climits',
    \ 'clocale',
    \ 'cmath',
    \ 'codecvt',
    \ 'complex',
    \ 'condition_variable',
    \ 'csetjmp',
    \ 'csignal',
    \ 'cstdarg',
    \ 'cstdbool',
    \ 'cstddef',
    \ 'cstdint',
    \ 'cstdio',
    \ 'cstdlib',
    \ 'cstring',
    \ 'ctgmath',
    \ 'ctime',
    \ 'cuchar',
    \ 'cwchar',
    \ 'cwctype',
    \ 'deque',
    \ 'exception',
    \ 'filesystem',
    \ 'forward_list',
    \ 'fstream',
    \ 'functional',
    \ 'future',
    \ 'hash_map',
    \ 'hash_set',
    \ 'initializer_list',
    \ 'iomanip',
    \ 'ios',
    \ 'iosfwd',
    \ 'iostream',
    \ 'istream',
    \ 'iterator',
    \ 'limits',
    \ 'list',
    \ 'locale',
    \ 'map',
    \ 'memory',
    \ 'mutex',
    \ 'new',
    \ 'numeric',
    \ 'ostream',
    \ 'queue',
    \ 'random',
    \ 'ratio',
    \ 'regex',
    \ 'scoped_allocator',
    \ 'set',
    \ 'shared_mutex',
    \ 'sstream',
    \ 'stack',
    \ 'stdexcept',
    \ 'streambuf',
    \ 'string',
    \ 'strstream',
    \ 'system_error',
    \ 'thread',
    \ 'tuple',
    \ 'typeindex',
    \ 'typeinfo',
    \ 'type_traits',
    \ 'unordered_map',
    \ 'unordered_set',
    \ 'utility',
    \ 'valarray',
    \ 'vector',
    \ 'xcomplex',
    \ 'xfacet',
    \ 'xfunctional',
    \ 'xhash',
    \ 'xiosbase',
    \ 'xlocale',
    \ 'xlocbuf',
    \ 'xlocinfo',
    \ 'xlocmes',
    \ 'xlocmon',
    \ 'xlocnum',
    \ 'xloctime',
    \ 'xmemory',
    \ 'xmemory0',
    \ 'xstddef',
    \ 'xstring',
    \ 'xtr1common',
    \ 'xtree',
    \ 'xutility',
    \ 'xxatomic'
    \ ]

let s:pattern = '*/include/{' . join(s:headers, ',') . '}'
"let s:pattern = '*/include/*/{[^\.]\\\{2,30\}}'
"echo s:pattern
execute "autocmd BufNewFile,BufRead" s:pattern "set ft=cpp"

