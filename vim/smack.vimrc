runtime vimrc

augroup SmackFormatting
    autocmd!
    autocmd BufRead     * call SetupExtnFf(expand("<afile>"), "BufRead")
    autocmd BufWinEnter * call SetupExtnFf(expand("<afile>"), "BufWinEnter")
    autocmd FileType    * call SetupExtnFf(expand("<afile>"), "FileType")
    autocmd VimEnter    * call SetupExtnFf(expand("<afile>"), "VimEnter")
    "autocmd FileType,VimEnter    *.py                    let b:fix_formatting_coalesce_blank_lines = 0
    "autocmd FileType,VimEnter    *.{json,[tj]s,svelte}   let b:fix_formatting_retab = 0
augroup END

function! SetupExtnFf(filename, cmd)
    if exists("b:extn_ffs_done")
        "echo "Already did the extension setup. (". a:cmd . ": " . a:filename . ")"
        return
    endif

    let l:extn = fnamemodify(a:filename, ":e")
    if l:extn =~ '\vpy'
        "echo "Fix Formatting: disable coalesce blank lines in " . l:extn . " file (" . a:cmd . ")"
        let b:fix_formatting_coalesce_blank_lines=0
    elseif l:extn =~ '\vjson|[tj]s|svelte'
        "echo "Fix Formatting: disable retabstopping in " . l:extn ." file (" . a:cmd . ")"
        let b:fix_formatting_retab=0
    else
        "echo "Didn't match" l:extn
    endif

    let b:extn_ffs_done = 1
endfunction

function! LocalFixFormatting(filename)
endfunction
