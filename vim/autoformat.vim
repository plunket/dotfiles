if exists('b:did_buffer_format_determination')
    finish
endif
let b:did_buffer_format_determination = 1

let s:print_info = 0

function! FigureOutBufferFixups()
    let l:view = winsaveview()
    let l:num_lines = line('$')

    try
        let l:num_tabs_to_find = l:num_lines / 2
        if l:num_tabs_to_find > 20
            let l:num_tabs_to_find = 20
        endif
        call cursor(1, 1)
        while (l:num_tabs_to_find > 0) && (search('^\t', 'e') != 0)
            let l:num_tabs_to_find = l:num_tabs_to_find - 1
        endwhile
        if l:num_tabs_to_find == 0
            let b:fix_formatting_retab = 0
            setlocal noet
        elseif l:num_tabs_to_find
            let b:fix_formatting_retab = 1
            setlocal et
        endif

        let l:num_multinewlines_to_find = 5
        call cursor(1, 1)
        while (l:num_multinewlines_to_find > 0) && (search('\n\{3,}', 'e') != 0)
            let l:num_multinewlines_to_find = l:num_multinewlines_to_find - 1
        endwhile
        if l:num_multinewlines_to_find == 0
            let b:fix_formatting_coalesce_blank_lines = 0
        else
            let b:fix_formatting_coalesce_blank_lines = 1
        endif

        let l:num_trailingspace_to_find = 5
        call cursor(1, 1)
        while (l:num_trailingspace_to_find > 0) && (search('\s\+$', 'e') != 0)
            let l:num_trailingspace_to_find = l:num_trailingspace_to_find - 1
        endwhile
        if l:num_trailingspace_to_find == 0
            let b:fix_formatting_delete_trailing_spaces = 0
        else
            let b:fix_formatting_delete_trailing_spaces = 1
        endif

        if s:print_info
            echo "tablines: " . l:num_tabs_to_find . " (" . b:fix_formatting_retab . "), "
              \. "multiblanks: " . l:num_multinewlines_to_find . " (" . b:fix_formatting_coalesce_blank_lines . "), "
              \. "trailingspace: " . l:num_trailingspace_to_find . " (" . b:fix_formatting_delete_trailing_spaces . ")"
        endif

        let b:did_buffer_format_determination =
            \  'rt:' . b:fix_formatting_retab . " "
            \. 'nl:' . b:fix_formatting_coalesce_blank_lines . " "
            \. 'ts:' . b:fix_formatting_delete_trailing_spaces

    finally
        call winrestview(l:view)
    endtry
endfunction

call FigureOutBufferFixups()

