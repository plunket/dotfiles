"runtime vimrc

if argc() == 0
    "lcd E:/Projects/EnvelopVR/main/sdk
    lcd E:/Projects/EnvelopVR/main/sdk_release
    call SetUpPath(getcwd())
endif

nnoremap <buffer> <F2> :call ConvertToAccessor()<CR>
nnoremap <buffer> <S-F2> :call ConvertToInitializer()<CR>
nnoremap <buffer> <C-F2> :call SplitArgListIntoMembers()<CR>
vnoremap <buffer> <F2> :'<,'>call ConvertToAccessor()<CR>
vnoremap <buffer> <S-F2> :'<,'>call ConvertToInitializer()<CR>

nnoremap <buffer> <F3> :silent !p4 edit %<CR>:w!<CR>:e<CR>
nnoremap <buffer> <S-F3> :w<CR>:silent !p4 add %<CR>
nnoremap <buffer> <C-F3> :silent !p4 diff %<CR>
nnoremap <buffer> <S-C-F3> :silent !p4vc timelapse %<CR>

nnoremap <buffer> <F8> :$tabe e:/dotfiles/vim/vimrc<CR>:sp e:/dotfiles/vim/envelop.vimrc<CR>

iabbrev ### <C-R>=GetIncludeLine()<CR>

" convert "SomeType mVariable" to "SomeType getVariable() const { return mVariable; }"
function! ConvertToAccessor() range
    execute a:firstline .','. a:lastline
         \. 's/\v^(\s*)(.*) m(\i*);/\1\2 get\3() const\r\1{\r\1    return m\3;\r\1}\r/'
endfunction

" convert "SomeType mVariable" to "mVariable(variable),"
function! ConvertToInitializer() range
    execute a:firstline . ','. a:lastline
         \. 's/\v^(\s*)(.*) (m\i*);/'
         \.     '\=submatch(1)."    ".submatch(3)."(".MemberNameToArgName(submatch(3))."),"'
         \. '/'
    if getline(a:lastline+1) =~ "{"
        execute a:lastline . 's/,//'
    endif
endfunction

" convert "some_argument" into "mSomeArgument"
function! ArgNameToMemberName(arg)
    let l:member = substitute(a:arg, '\v([^_]*)_?', '\u\1', 'g')
    return 'm'. l:member
endfunction

" convert "mSomeMember" into "some_member"
function! MemberNameToArgName(member)
    if (a:member[0] == 'm')
        let l:arg = substitute(a:member, '\v(\u+\U*)', '_\l\1', 'g')
        return l:arg[2:]
    else
        return a:member
    endif
endfunction

" Take the parameter list of the current function and turn all of the arguments into member
" variables and initialize them.
function! SplitArgListIntoMembers()
    let l:currentLine = line('.')

    " select everything between the parentheses
    normal ^f(vi)v

    " get all of the stuff that was just selected
    let l:arglist = GetVisualSelection()

    " split up args based on commas
    let l:args = split(l:arglist, ',\_s*')

    if getline('.') !~ ':\s*$'
        s/)\s*$/) :/
    endif

    " build the initializers...
    for l:a in l:args
        execute "normal o"
             \. substitute(l:a, '.* \(\i*\)$', '\=ArgNameToMemberName(submatch(1))."(".submatch(1)."),"', '')
    endfor

    " delete the last comma
    normal $x

    " go to the bottom of the class definition then up one line
    normal vaBv'>k

    for l:a in l:args
        execute "normal o"
             \. substitute(l:a, '\(\i*\)$', '\=ArgNameToMemberName(submatch(1))', '')
             \. ';'
    endfor

    " return the cursor to where we started
    execute l:currentLine
endfunction

" remove the FixFormatting function as defined in vimrc because we don't want to strip whitespace
function! FixFormatting(filename)
    " replace tabs with spaces
    setlocal expandtab
    setlocal tabstop=4
    retab
endfunction

function! GetIncludeLine()
    let l:myName = expand("%:p")
    let l:mySplitName = split(l:myName, '[\\/]')
    let l:includeName = expand("#:p:r") . '.h'
    let l:includeSplitName = split(l:includeName, '[\\/]')

    let l:myEntry = get(l:mySplitName, 0)
    let l:includeEntry = get(l:includeSplitName, 0)
    let l:currentIndex = 0
    while (l:currentIndex < len(l:includeSplitName)) && (l:myEntry == l:includeEntry)
        let l:currentIndex = l:currentIndex + 1
        let l:myEntry = get(l:mySplitName, l:currentIndex)
        let l:includeEntry = get(l:includeSplitName, l:currentIndex)
    endwhile

    if l:currentIndex < len(l:includeSplitName) - 1
        let l:filename = join(l:includeSplitName[l:currentIndex+1 : ], '/')
        let l:openQuote = '<'
        let l:closeQuote = '>'
    else
        let l:filename = l:includeSplitName[-1]
        let l:openQuote = '"'
        let l:closeQuote = '"'
    endif
    return '#include ' . l:openQuote . l:filename . l:closeQuote
endfunction
