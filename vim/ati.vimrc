runtime vimrc

if has('win32') || has('win64')
    let s:workDir = 'C:/Aristocrat'
elseif has('win32unix')
    let s:workDir = '/cygdrive/c/Aristocrat'
endif

" Look for a file named 'tags' in the current directory and walking up to the filesystem root.
set tags=tags;

" Add the externals, in case we're hunting around somewhere crazy and we just want to see APIs.
execute 'set tags+=' . s:workDir . '/dev/Trunk-2.2/External/tags'

" remove one of the extra "current dir" searches
set path-=

if $COMPUTERNAME ==? "TPLUNKET01-LW7"
    set backup      " keep a backup file
    set swapfile
else
    set nobackup
    set noswapfile
endif

function! FigureOutBranch(filepath)
    " If there's a tag file, assume it's at our "project root".
    " But skip this if only the External tags are in the list because that's not probably not
    " actually where we are...
    if len(tagfiles()) > 1
        let l:tagfile = get(tagfiles(), 0, "")
        "echo l:tagfile
        let l:tagfile = substitute(l:tagfile, '\\', '/', 'g')
        " What we consider branches can only be in the Aristocrat hierarchy.
        if l:tagfile =~? '/Aristocrat/'
            "echo 'tagfile: "' . l:tagfile . '"'
            if l:tagfile =~ '/tags$'
                return substitute(l:tagfile, '/tags$', '', '')
            endif
        endif
    endif

    return ""
endfunction

"let s:defaultDirectory = s:workDir . '/dev/PlatformArchitecture'
if $MAIN_DEV_BRANCH != ''
    if has('win32') || has('win64')
        let s:defaultDirectory = substitute($MAIN_DEV_BRANCH, '\\', '/', 'g')
    elseif has('win32unix')
        silent let s:defaultDirectory = system('cygpath ' . shellescape($MAIN_DEV_BRANCH))[:-2]
    endif
else
    let s:defaultDirectory = s:workDir . '/dev'
endif

" if there are no command line parameters, change into a useful directory
" so that :find can work
if argc() == 0
    let s:branch = FigureOutBranch(getcwd())
else
    let s:branch = FigureOutBranch(argv(0))
endif
"echo "Branch initialized to" s:branch

function! SetPath(forFile)
    if a:forFile == s:branch
        let l:setCmd = "set"
        let l:branch = s:branch
    else
        let l:setCmd = "setlocal"
        let l:branch = FigureOutBranch(a:forFile)
    endif

    if l:branch != ""
        " set the path for :find, gF, etc.
        let l:pathcmd = l:setCmd . ' path=' . l:branch . '/Tools/**,' . l:branch . '/Runtime/**'
        "echo l:pathcmd
        execute l:pathcmd
    endif
endfunction

function! GetBranch()
    "echom "Getting branch:" s:branch
    return s:branch
endfunction

let g:Ag_get_project_root = function("GetBranch")
let g:ag_working_path_mode = 'r'

function! FixBranch()
    let s:branch = FigureOutBranch(expand("%:p"))
    call SetPath(s:branch)
    echom "Branch set to" s:branch
endfunction

if len(s:branch) != 0
    call SetPath(s:branch)
endif

augroup ATI
    autocmd!
    autocmd BufEnter    *   call SetPath(fnamemodify(expand("<afile>"), ":p"))
    autocmd BufEnter    *   call SetUpBufferSettings(fnamemodify(expand("<afile>"), ":e"))
    autocmd BufEnter    *   runtime autoformat.vim
augroup END

nnoremap <F2> IERR(<Esc>f(%a)<Esc>
"nnoremap <F3> :vimgrep // <C-R>=GetBranch()<Enter>/**/*.{cpp,h,c}<C-Left><C-Left><Right>
nnoremap <F3> :Ag -G "\.(cpp\|h\|c)" "" <C-R>=GetBranch()<Enter><C-Left><C-Left><Right>
if has('win32') || has('win64')
    nnoremap <F8> :$tabe c:/dotfiles/vim/vimrc<CR>:sp c:/dotfiles/vim/ati.vimrc<CR>
elseif has('win32unix')
    let s:dotfiles_path = system('cygpath c:/dotfiles/vim')[:-2] " remove the newline
    execute 'nnoremap <F8> '
        \ . ':$tabe ' . s:dotfiles_path . '/vimrc<CR>'
        \ . ':sp ' . s:dotfiles_path . '/ati.vimrc<CR>'
endif

" Override the basic GetRelatedFileList coming from vimrc.
function! GetRelatedFileList()
    let l:thisDir = expand("%:p:h")
    let l:thisDir = substitute(l:thisDir, '\', '/', 'g')
    if (l:thisDir =~? '/s\(rc\|ource\)\>')
        let l:searchPath = substitute(l:thisDir, '/s\(rc\|ource\)\>', '/include', '') . ',' . l:thisDir
        let l:thisBase = expand("%:t:r:r:r:r:r") . '.*'
        let l:files = globpath(l:searchPath, l:thisBase)
    elseif (l:thisDir =~? '/include\>')
        let l:searchPath = substitute(l:thisDir, '/include\>', '/src', '') . ',' . substitute(l:thisDir, '/include\>', '/source', '') . ',' . l:thisDir
        let l:thisBase = expand("%:t:r:r:r:r:r") . '.*'
        let l:files = globpath(l:searchPath, l:thisBase)
    else
        let l:thisPath = expand("%:p:r:r:r:r:r") . '.*'
        let l:files = glob(l:thisPath)
    endif

    return split(l:files, '[\r\n]\+')
endfunction

"Override the generic alternate file header name expansion.
function! GetAlternateFileHeaderName(extension)
    let l:altfile = expand("#:p")
    let l:altfile = substitute(l:altfile, '\', '/', 'g')
    if l:altfile =~? '.*/eastl/'
        return substitute(l:altfile, '.*/eastl/', 'eastl/', '')
    else
        return fnamemodify(l:altfile, ":t:r") . '.h'
    endif
endfunction

function! SetUpCrossbridgeErrorFormat()
    let &errorformat =
        \  '%*[^"]"%f"%*\D%l: %m,'
        \. '"%f"%*\D%l: %m,'
        \. '%-G%f:%l: (Each undeclared identifier is reported only once,'
        \. '%-G%f:%l: for each function it appears in.),'
        \. '%-GIn file included from %f:%l:%c:,'
        \. '%-GIn file included from %f:%l:%c\,,'
        \. '%-GIn file included from %f:%l:%c,'
        \. '%-GIn file included from %f:%l,'
        \. '%-G%*[ ]from %f:%l:%c,'
        \. '%-G%*[ ]from %f:%l:,'
        \. '%-G%*[ ]from %f:%l\,,'
        \. '%-G%*[ ]from %f:%l,'
        \. '%f:%l:%c:%m,'
        \. '%f(%l):%m,'
        \. '%f:%l:%m,'
        \. '"%f"\, line %l%*\D%c%*[^ ] %m,'
        \. '%D%*\a[%*\d]: Entering directory %*[`'']%f'','
        \. '%X%*\a[%*\d]: Leaving directory %*[`'']%f'','
        \. '%D%*\a: Entering directory %*[`'']%f'','
        \. '%X%*\a: Leaving directory %*[`'']%f'','
        \. '%DMaking %*\a in %f,'
        \. '%f|%l| %m'
endfunction

function! OpenErrorPreviewWindow()
    cwindow
    setlocal colorcolumn=0
    setlocal wrap
    wincmd p
endfunction

"nnoremap <F5> :call LoadAsErrorFile(expand("%:p"))<CR>

function! LoadAsErrorFile(file)
    call SetUpCrossbridgeErrorFormat()
    execute "cfile" a:file
    call OpenErrorPreviewWindow()
endfunction

let g:fix_formatting_coalesce_blank_lines = 8

function! SetUpBufferSettings(extension)
    "echo "Setting up buffer settings."
    let numLines = line("$")
    let numTabLines = numLines / 4
    if numTabLines > 20
        let numTabLines = 20
    elseif numTabLines < 3
        let numTabLines = 3
    endif

    let l:winview = winsaveview()
    let numTabLines += 1 " push it up by one since we have one 'free' trip through this loop
    let nextTabLine = 1
    while (numTabLines > 0) && (nextTabLine != 0)
        call cursor(nextTabLine, 1)
        let nextTabLine = search('^\s*\t', 'W')
        "echo nextTabLine
        let numTabLines -= 1
    endwhile
    call winrestview(l:winview)

    if numTabLines == 0
        let b:fix_formatting_retab = 0
    endif
endfunction

function! Build(platform, flavor, mechanism, project)
    let l:projects = { 'GDK'  : '\Runtime\core\GDK\GDK_2015.sln',
                     \ 'Tools': '\Tools\source\win32\MercuryTools_2015.sln' }

    let l:branch = FigureOutBranch(expand('%:p'))

    let l:projectpath = l:branch . l:projects[a:project]
    let l:projectdir = fnamemodify(l:projectpath, ':h')
    let l:project = fnamemodify(l:projectpath, ':t')
    let l:cwd = getcwd()
    "echo "branch:" l:branch
    "echo l:projectpath
    "echo "projectdir:" l:projectdir
    "echo l:project
    "echo l:cwd

    call EnsurePreviewWindow()
    let l:projectdir = substitute(l:projectdir, "\/", "\\", "g")
    "echo l:projectdir
    execute "lcd" l:projectdir
    "echo 'should have changed to above path:' getcwd()
    "echo 'proj:'.l:project 'mech:'.a:mechanism 'flav:'.a:flavor 'plat:'.a:platform
    execute "make" l:project a:mechanism a:flavor a:platform "alwaysoutput"

    execute "cd" l:cwd
endfunction

function! SetUpBuildCommands()
    let l:prologue = 'nnoremap <LocalLeader>'
    let l:mechanisms = [ ['', ''], ['r', 'Rebuild'] ]
    let l:projects = [ ['g', 'GDK'], ['t', 'Tools'] ]
    let l:platforms = [ ['', '64'], ['3', '32'] ]
    let l:compilecommands = [ ['d', 'Debug'], ['r', 'Release'] ]

    execute prologue . 'r <Nop>'

    for l:m in l:mechanisms
        for l:pr in l:projects
            execute prologue . l:m[0] . l:pr[0] . " <Nop>"
            execute prologue . l:m[0] . l:pr[0] . "3 <Nop>"

            for l:pl in l:platforms
                for l:c in l:compilecommands
                    execute prologue . l:m[0] . l:pr[0] . l:pl[0] . l:c[0]
                        \" :call Build('" . l:pl[1] . "', '" . l:c[1] . "', '" . l:m[1] . "', '" . l:pr[1] . "')<CR>"
                endfor
            endfor
        endfor
    endfor

    let &makeprg = s:workDir . '\ext\buildsln.bat'

    call SetVisualStudioErrorFormat()
endfunction

call SetUpBuildCommands()

function! InsertCopyright()
    let l:year = strftime("%Y")
    return
        \  "/**\n"
        \. "Copyright Aristocrat Technologies Incorporated, " . l:year . ". All rights reserved.\n"
        \. "/"
endfunction

iabbrev <expr> copy# InsertCopyright()

function! GenerateSkeleton(filename)
    execute "normal A\<C-R>=InsertCopyright()\<Enter>\<Enter>\<Enter>"

    let l:extension = fnamemodify(a:filename, ":e")
    if l:extension =~ "c.*"
        execute 'normal O#include "' . fnamemodify(a:filename, ":r") . '.h"'
    else
        normal O#pragma once
    endif
    normal Go
endfunction

