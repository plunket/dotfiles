" Vim indent file
" Language: MAXscript files
" Version:  1
" Last Change:  2018 January 2
" Maintainer:   Tom Plunket (tom@mightysprite.com)
" Adapted from DOS Batch indent script by Geoff Wood
"
" Very simple indenting for MAXscript files, just
" - generally keep the indent of the previous line
" - if previous line is not a comment and has ( and no matching ),
"   increase indent
" - if this line starts with ), decrease indent
"
" Cut down and modified from Erik Janssen's awk.vim
"
"  History:
"  tp 2-jan-19 created

" Only load this indent file when no other was loaded.
if exists("b:did_indent")
    finish
endif
let b:did_indent = 1

setlocal indentexpr=GetMxsIndent()
setlocal indentkeys=0(,0),!^F,o,O

" Only define the function once.
if exists("*GetMxsIndent")
  finish
endif

function! GetMxsIndent()
  let this_line = getline(v:lnum)

  " Find previous line and get its indentation
  let prev_lineno = s:Get_prev_line( v:lnum )
  if prev_lineno == 0
    return 0
  endif

  let prev_prev_lineno = s:Get_prev_line( prev_lineno )
  if prev_prev_lineno > 0
    let prev_prev_line = getline( prev_prev_lineno )
  else
    let prev_prev_line = ""
  endif

  let prev_line = getline( prev_lineno )
  let ind = indent( prev_lineno )

  " Decrease indent if this line starts with a ')'
  if this_line =~ '^\s*).*'
    if prev_line !~ '^\s*([^)]*$'
      let ind = ind - &sw
    endif

  " Increase indent if the previous line contains an unmatched '('
  elseif prev_line =~ '.*([^)]*$'
    let ind = ind + &sw

  " Decrease indent if this line starts with a ')'
  elseif this_line =~ '^\s*).*'
    let ind = ind - &sw

  elseif this_line =~ '.*([^)]*$'
    " indent like the previous line says

  " Increase indent if previous line contains 'then' or 'do'
  elseif (prev_line =~ '.*\(then\|do\)\s*\(--.*\)\?$')
    let ind = ind + &sw

  " But decrease indent if previous-previous line contains 'then' or 'do'
  elseif prev_prev_line =~ '.*\(then\|do\)\s*\(--.*\)\?$'
    let ind = indent( prev_prev_lineno )
  endif

  return ind
endfunction

" Get previous relevant line. Search back until a line is that is no
" comment or blank and return the line number
function! s:Get_prev_line( lineno )
  let lnum = a:lineno - 1
  let data = getline( lnum )
  while lnum > 0 && (data =~? '^\s*--' || data =~ '^\s*$')
    let lnum = lnum - 1
    let data = getline( lnum )
  endwhile
  return lnum
endfunction

