#!/bin/bash
# Put (or symlink) this in ~/.config/git/git-prompt.sh -- symlink doesn't work with test -f
# It is sourced by /etc/profile.d/git-prompt.sh (on Git-Windows-bash, anyway)
# On WSL it needs to be sourced from the .profile.

PS1='\[\033]0;${GNOME_TERMINAL_PROFILE_NAME:+($GNOME_TERMINAL_PROFILE_NAME) }$PWD\007\]'       # set window title
PS1="$PS1"'\n'                  # new line
PS1="$PS1"'\[\033[32m\]'        # change to green
PS1="$PS1"'\@ '                 # current-time<space>
PS1="$PS1"'\[\033[33m\]'        # change to brownish yellow
PS1="$PS1"'\w'                  # current working directory

if [[ -z $(declare -F __git_ps1) && -z "$WINELOADERNOEXEC" ]]
then
    GIT_EXEC_PATH="$(git --exec-path 2>/dev/null)"
    COMPLETION_PATH="${GIT_EXEC_PATH%/libexec/git-core}"
    COMPLETION_PATH="${COMPLETION_PATH%/lib/git-core}"
    COMPLETION_PATH="$COMPLETION_PATH/share/git/completion"
    if test -f "$COMPLETION_PATH/git-prompt.sh"
    then
        . "$COMPLETION_PATH/git-completion.bash"
        . "$COMPLETION_PATH/git-prompt.sh"
    fi
fi

if [[ $(declare -F __git_ps1) ]]
then
    PS1="$PS1"'\[\033[36m\]'    # change color to cyan
    PS1="$PS1"'`__git_ps1`'     # bash function
fi

PS1="$PS1"'\[\033[0m\]'             # change color
PS1="$PS1"'\n'                      # new line
PS1="$PS1"'\$ '                     # prompt: always $

