set expandtab
set tabstop=4
set shiftwidth=4
set textwidth=100

let b:fix_formatting_coalesce_blank_lines=1
let b:fix_formatting_retab=1
